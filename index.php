<?php

use App\Classes\First;

require 'vendor/autoload.php';

$maVariable = 'bloup';

$nbEntier = 1;
$nbVirgule = 1.2;
$booleen = true;
$tableau = [1,2,3,4];

if ($maVariable == 'bloup') {
    echo 'maVariable contient bloup';
} elseif ($booleen) {
    echo 'maVariable ne contient pas bloup mais un booléen';
}

for ($i=0; $i < 10 ; $i++) { 
    echo $i;
}

foreach ($tableau as $index => $iterator) {
    echo $iterator;
}

$instance = new First('truc');
echo $instance->prop1;


//echo makeHTML('bloup');


function makeHTML (string $text): string {
    return '<p>' . $text . '</p>';
};


