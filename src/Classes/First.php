<?php

namespace App\Classes;

class First {
    public $prop1;
    protected $prop2;
    private $prop3;

    public function __construct(string $prop3) {
        $this->prop1 = 'pepito';
        $this->prop2 = 'toto';
        $this->prop3 = $prop3;
    }
}