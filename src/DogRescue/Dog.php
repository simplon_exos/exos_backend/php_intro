<?php

namespace App\DogRescue;

class Dog {
    private $name;
    private $birthdate;

    public function __construct(string $name, \DateTime $birthdate)
    {
        $this->name = $name;
        $this->birthdate = $birthdate;
    }
    /**
     * Méthode accesseur permettant la lecture de la
     * propriété name du chien
     */
    public function getName():string {
        return $this->name;
    }

    /**
     * Méthode accesseur permettant la lecture de la
     * propriété birthdate du chien
     */
    public function getBirthdate(): \DateTime {
        return $this->birthdate;
    }

    /**
     * Méthode accesseur permettant l'ecriture/la modification
     * de la propriété name du chien
     */
    public function setName(string $name): void {
        $this->name = $name;
    }
    /**
     * Méthode générant le HTML du chien
     */
    public function draw(): string {
        
        return '<article><h3>Name : ' . $this->name . '</h3><p>Birthdate : ' . $this->birthdate->format('d/m/Y') . '</p></article>';
    }
}
