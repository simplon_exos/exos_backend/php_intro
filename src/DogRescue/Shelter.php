<?php

namespace App\DogRescue;

class Shelter
{
    /**
     * @var Dog[] liste des chiens
     */
    private $list;

    public function __construct()
    {
        $this->list = [];
    }

    /**
     * Méthode permettant d'ajouter un chien dans la liste
     */

    public function addDog(Dog $dog): void
    {
        // array_push($this->list, $dog);
        $this->list[] = $dog;
    }

    /**
     * Méthode permettant d'afficher la liste des chiens
     */
    public function draw(): string
    {
        $html = '<section class="shelter">';
        foreach ($this->list as $dog) {
            $html .= $dog->draw();
        }
        $html .= '</section>';
        return $html;
    }
}
