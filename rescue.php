<?php

use App\DogRescue\Dog;
use App\DogRescue\Shelter;

require 'vendor/autoload.php';

$dog1 = new Dog('Disco', new DateTime('2008-09-11'));

$dog2 = new Dog('Titou', new DateTime('2018-08-23'));

$dog3 = new Dog('Khiba', new DateTime('2005-04-01'));


// echo $instance->getName();

// echo $instance->getBirthdate();

// echo $instance->setName('Titou');

// echo $instance->getName();

// echo $dog1->draw();
// echo $dog2->draw();


$shelter = new Shelter();

$shelter->addDog($dog1);
$shelter->addDog($dog2);
$shelter->addDog($dog3);


echo $shelter->draw();
